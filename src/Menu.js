import { settings } from './settings';


class Menu extends Phaser.Scene {
    constructor() {
        super({key: settings.scenes.menu});
    }

    preload() {
        this.load.image(settings.sprites.playButton, 'assets/play.png');

        this.load.image(settings.sprites.stone, 'assets/stone.png');

        this.load.image(settings.sprites.gameover, 'assets/gameover.png');

        this.load.spritesheet(
            settings.sprites.tail,
            'assets/tail.png', 
            {frameWidth: 64, frameHeight: 48});

        this.load.spritesheet(
            settings.sprites.death,
            'assets/death.png', 
            {frameWidth: 32, frameHeight: 32});

    }

    create() {
        this.anims.create({
            key: settings.anims.left,
            frames: this.anims.generateFrameNumbers(settings.sprites.tail, {start: 0, end: 3}),
            frameRate: settings.frameRate,
            repeat: -1,
        });

        this.anims.create({
            key: settings.anims.right,
            frames: this.anims.generateFrameNumbers(settings.sprites.tail, {start: 4, end: 7}),
            frameRate: settings.frameRate,
            repeat: -1,
        });

        this.add.image(
            settings.gameWidth / 2,
            settings.gameHeight / 2,
            settings.sprites.playButton
        ).setInteractive().on('pointerup', () => {
            this.scene.start(settings.scenes.game);
        })

        let player = this.add.sprite(
            settings.gameWidth / 2 - 20,
            settings.gameHeight / 2,
            settings.sprites.tail);
        player.setRotation(Phaser.Math.DegToRad(90));
        player.anims.play(settings.anims.left);
    }
}

export { Menu }
