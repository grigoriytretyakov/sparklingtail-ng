import 'phaser';

import { settings } from './settings';

import { Game } from './Game';
import { Menu } from './Menu';
import { GameOver } from './GameOver';

function resizeGame(game) {
    let canvas = document.querySelector('canvas');
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    if (windowWidth < game.config.width || windowHeight < game.config.height) {
        let windowRatio = windowWidth / windowHeight;
        let gameRatio = game.config.width / game.config.height;
        if (windowRatio < gameRatio) {
            canvas.style.width = windowWidth + "px";
            canvas.style.height = (windowWidth / gameRatio) + "px";
        }
        else {
            canvas.style.width = (windowHeight * gameRatio) + "px";
            canvas.style.height = windowHeight + "px";
        }
    }
    else {
        canvas.style.width = game.config.width + "px";
        canvas.style.height = game.config.height + "px";
    }
}


window.onload = () => {
    //document.body.style.backgroundColor = '#' + settings.backgroundColor.toString(16);

    document.querySelector('h1').remove();

    const gameConfig = {
        type: Phaser.AUTO,

        width: settings.gameWidth,
        height: settings.gameHeight,
        backgroundColor: settings.backgroundColor,

        physics: {
            default: 'arcade',
            arcade: {
                gravity: {
                    x: 0,
                    y: 0
                },
                debug: false,
            }
        },

        scene: [
            Menu,
            Game,
            GameOver,
        ]
    };

    let game = new Phaser.Game(gameConfig);

    window.focus();
    resizeGame(game);

    window.addEventListener('resize', () => { resizeGame(game) });
}
