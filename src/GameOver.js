import { settings } from './settings';


class GameOver extends Phaser.Scene {
    constructor() {
        super({key: settings.scenes.gameover})
    }

    create() {
        this.add.sprite(
            settings.gameWidth / 2,
            settings.gameHeight / 2,
            settings.sprites.gameover);

        this.input.on("pointerup", () => {
            this.scene.start(settings.scenes.game);
        }, this);
    }
}

export { GameOver }
