import { settings } from './settings';


class Game extends Phaser.Scene {
    constructor() {
        super({key: settings.scenes.game});
    }

    create() {
        this.createWall();
        this.createTail();
        this.createDeath();

        this.input.on("pointerup", this.handleTouch, this);
        this.input.keyboard.on('keydown', this.handleKey, this);
        this.cameras.main.on('camerafadeoutcomplete', () => {
            this.scene.start(settings.scenes.gameover)
        })
    }

    update(time, delta) {
        if (this.player.y < 0 || this.player.y > settings.gameHeight 
            || this.player.x < 0 || this.player.x > settings.gameWidth) {
            this.gameover();
            return;
        }

        if (this.player.body.velocity.y !== 0) {
            let velocity = this.player.body.velocity.y * 0.97;
            if (Math.abs(velocity) < 5) {
                velocity = 0;
            }
            this.player.setVelocityY(velocity);
        }

        this.wait -= delta;
        if (this.wait <= 0) {
            this.wait = Phaser.Math.Between(
                settings.minDeathFireTime, settings.maxDeathFireTime);

            this.fireDeath();
        }

		this.physics.world.collide(this.walls, this.deaths);

		this.physics.world.collide(this.player, this.deaths, (_, death) => {
            this.cameras.main.shake(60);

            if (death.deathType == settings.anims.spikes) {
                this.gameover();
                return;
            }
        });

        this.deaths = this.deaths.filter((item) => {
            const isOutside = item.x < -settings.livePadding
                || item.x > (settings.gameWidth + settings.livePadding)
                || item.y < -settings.livePadding
                || item.y > (settings.gameHeight + settings.livePadding);
            if (isOutside) {
                item.destroy();
            }
            return !isOutside;
        });
    }

    gameover() {
        this.cameras.main.fade(900, 5, 5, 2);
    }

    handleTouch(e) {
        if (e.upX < settings.gameWidth / 2) {
            this.toLeft();
        } 
        else {
            this.toRight();
        }
    }

    handleKey(e) {
        switch (e.code) {
            case 'KeyA':
            case 'ArrowLeft':
                this.toLeft();
                break;
            case 'KeyD':
            case 'ArrowRight':
                this.toRight();
                break;
        }
    } 
    toLeft() {
        if (this.player.x > settings.gameWidth / 2) {
            this.player.setX(settings.gameWidth / 2 - settings.wallWidth / 2 - 32)
            this.player.anims.play(settings.anims.left);
        }
    }

    toRight() {
        if (this.player.x < settings.gameWidth / 2) {
            this.player.setX(settings.gameWidth / 2 + settings.wallWidth / 2 + 32)
            this.player.anims.play(settings.anims.right);
        }
    }

    createDeath() {
        this.deaths = [];

        this.anims.create({
            key: settings.anims.spikes,
            frames: this.anims.generateFrameNumbers(settings.sprites.death, {start: 0, end: 3}),
            frameRate: settings.deathFrameRate,
            repeat: -1,
        });
        this.anims.create({
            key: settings.anims.stones,
            frames: this.anims.generateFrameNumbers(settings.sprites.death, {start: 4, end: 7}),
            frameRate: settings.deathFrameRate,
            repeat: -1,
        });

        this.wait = Phaser.Math.Between(
            settings.minDeathFireTime, settings.maxDeathFireTime);
    }

    fireDeath() {
        const TOP = 0, RIGHT = 1, BOTTOM = 2, LEFT = 3;
        const FALL = 0, JUMP = 1, HIT = 2;

        let startX, startY;
        let types;

        switch(Phaser.Math.RND.pick([TOP, RIGHT, BOTTOM, LEFT])) {
            case TOP:
                startX = Phaser.Math.Between(0, settings.gameWidth);
                startY = -settings.livePadding;
                types = [FALL];
                break;

            case RIGHT:
                startX = settings.gameWidth + settings.livePadding;
                startY = Phaser.Math.Between(0, settings.gameHeight);
                types = [HIT, JUMP];
                break;

            case BOTTOM:
                startX = Phaser.Math.Between(0, settings.gameWidth);
                startY = settings.gameHeight + settings.livePadding;
                types = [FALL];
                break;

            case LEFT:
                startX = settings.livePadding;
                startY = Phaser.Math.Between(0, settings.gameHeight);
                types = [HIT, JUMP];
                break;
        }

        let death = this.physics.add.sprite(startX, startY, settings.sprites.deaths);
        death.setBounce(1);

        switch(Phaser.Math.RND.pick(types)) {
            case FALL:
                death.setVelocity(
                    0, (-Math.sign(startY)) * Phaser.Math.Between(
                        settings.minFallVelocity, settings.maxFallVelocity));
                break;

            case JUMP:
                let xsign = startX < settings.gameWidth / 2 ? 1 : -1;
                let ysign =  startY < settings.gameHeight / 2 ? 1 : -1;

                death.setGravity(
                    xsign * Phaser.Math.Between(settings.minJumpGravityX, settings.maxJumpGravityX),
                    ysign * Phaser.Math.Between(settings.minJumpGravityY, settings.maxJumpGravityY)
                );
                break;

            case HIT:
                let v = 300;
                if (startX < settings.gameWidth / 2) {
                    death.setVelocityX(v);
                }
                else {
                    death.setVelocityX(-v);
                }
                if (startY < settings.gameHeight / 2) {
                    death.setVelocityY(v);
                } 
                else {
                    death.setVelocityY(-v);
                }
                break;
        }

        switch(Phaser.Math.RND.pick([settings.anims.spikes, settings.anims.stones])) {
            case settings.anims.spikes:
                death.anims.play(settings.anims.spikes);
                death.deathType = settings.anims.spikes;
                break;

            case settings.anims.stones:
                death.anims.play(settings.anims.stones);
                death.deathType = settings.anims.stones;
                break;
        }

        this.deaths.push(death);
    }

    createWall() {
        this.walls = this.physics.add.staticGroup();

        let maxY = settings.gameHeight;
        for (let y = 0; y <= maxY; y += settings.wallHeight) {
            this.walls.create(settings.gameWidth / 2, y, settings.sprites.stone);
        }
    }

    createTail() {
        this.player = this.physics.add.sprite(
            settings.gameWidth / 2 - settings.wallWidth / 2 - 32,
            settings.gameHeight / 2,
            settings.sprites.tail);

        this.physics.add.collider(this.player, this.walls);

        this.player.anims.play(settings.anims.left);
    }
}

export { Game }
