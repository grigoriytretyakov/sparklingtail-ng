const gameRatio = 16 / 10; // h / w

const gameHeight = 900;
const gameWidth = gameHeight / gameRatio;

export const settings = {
    gameWidth,

    gameHeight,

    backgroundColor: 0x535D6C,

    frameRate: 8,

    scenes: {
        game: 'Game',
        menu: 'Menu',
        gameover: 'GameOver',
    },

    sprites: {
        tail: 'tail',
        stone: 'stone',
        death: 'death',
        playButton: 'playButton',
        gameover: 'gameover',
    },

    anims: {
        left: 'left',
        right: 'right',
        spikes: 'spikes',
        stones: 'stones',
    },

    wallHeight: 32,
    wallWidth: 32,

    minDeathFireTime: 100,
    maxDeathFireTime: 800,
    deathFrameRate: 8,

    livePadding: 30,
    minFallVelocity: 300,
    maxFallVelocity: 800,

    minJumpGravityX: 400,
    maxJumpGravityX: 700,
    minJumpGravityY: 100,
    maxJumpGravityY: 300,
};
